/*******************************************************************************************************************
 * Authors:   SanAndreasP
 * Copyright: SanAndreasP, SilverChiren and CliffracerX
 * License:   Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International
 *                http://creativecommons.org/licenses/by-nc-sa/4.0/
 *******************************************************************************************************************/
package de.sanandrew.mods.clonecraft.crafting;

import de.sanandrew.mods.clonecraft.util.CloneCraftReloaded;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraftforge.oredict.RecipeSorter;
import net.minecraftforge.oredict.RecipeSorter.Category;

public class RegistryRecipes
{
    public static void initialize() {
        RecipeSorter.register(CloneCraftReloaded.MOD_ID + ":fillTestTube", RecipeFullTestTube.class, Category.SHAPELESS, "after:minecraft:shapeless");

        CraftingManager.getInstance().getRecipeList().add(new RecipeFullTestTube());
    }
}
