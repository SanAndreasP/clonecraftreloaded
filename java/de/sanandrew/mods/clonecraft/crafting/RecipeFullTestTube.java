/*******************************************************************************************************************
 * Authors:   SanAndreasP
 * Copyright: SanAndreasP, SilverChiren and CliffracerX
 * License:   Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International
 *                http://creativecommons.org/licenses/by-nc-sa/4.0/
 *******************************************************************************************************************/
package de.sanandrew.mods.clonecraft.crafting;

import de.sanandrew.core.manpack.util.helpers.SAPUtils;
import de.sanandrew.mods.clonecraft.item.ItemSyringe;
import de.sanandrew.mods.clonecraft.item.ItemTestTube;
import de.sanandrew.mods.clonecraft.util.RegistryItems;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import net.minecraftforge.oredict.OreDictionary;

public class RecipeFullTestTube
        implements IRecipe
{
    private ItemStack syringe = new ItemStack(RegistryItems.syringe, 1, OreDictionary.WILDCARD_VALUE);
    private ItemStack testtube = new ItemStack(RegistryItems.testTube, 1, OreDictionary.WILDCARD_VALUE);

    @Override
    public boolean matches(InventoryCrafting invCrafting, World world) {
        boolean hasFullSyringe = false;
        boolean hasEmptyTestTube = false;
        NBTTagCompound nbt;
        ItemStack slotItem;

        for( int i = 0; i < invCrafting.getSizeInventory(); i++ ) {
            slotItem = invCrafting.getStackInSlot(i);
            if( SAPUtils.areStacksEqualWithWCV(slotItem, this.syringe) ) {
                nbt = slotItem.getTagCompound();
                if( nbt != null && !nbt.getBoolean(ItemSyringe.NBT_CONTAMINATED) && ItemSyringe.getSavedEntityClass(slotItem) != null ) {
                    hasFullSyringe = true;
                }
            } else if( SAPUtils.areStacksEqualWithWCV(slotItem, this.testtube) ) {
                nbt = slotItem.getTagCompound();
                if( nbt == null || (!nbt.getBoolean(ItemTestTube.NBT_CONTAMINATED) && ItemTestTube.getSavedEntityClass(slotItem) == null) ) {
                    hasEmptyTestTube = true;
                }
            } else if( slotItem != null ) {
                return false;
            }
        }

        return hasFullSyringe && hasEmptyTestTube;
    }

    @Override
    public ItemStack getCraftingResult(InventoryCrafting invCrafting) {
        ItemStack syringe = null;

        for( int i = 0; i < invCrafting.getSizeInventory(); i++ ) {
            ItemStack slotItem = invCrafting.getStackInSlot(i);
            if( SAPUtils.areStacksEqualWithWCV(slotItem, this.syringe) ) {
                syringe = slotItem;
                break;
            }
        }

        ItemStack fullTestTube = new ItemStack(RegistryItems.testTube, 1);
        NBTTagCompound nbt = new NBTTagCompound();

        nbt.setString(ItemTestTube.NBT_ENTITY_CLS, syringe.getTagCompound().getString(ItemSyringe.NBT_ENTITY_CLS));
        fullTestTube.setTagCompound(nbt);

        return fullTestTube;
    }

    @Override
    public int getRecipeSize() {
        return 2;
    }

    @Override
    public ItemStack getRecipeOutput() {
        return null;
    }
}
