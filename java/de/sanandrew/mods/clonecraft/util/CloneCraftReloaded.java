/*******************************************************************************************************************
 * Authors:   SanAndreasP
 * Copyright: SanAndreasP
 * License:   Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International
 *                http://creativecommons.org/licenses/by-nc-sa/4.0/
 *******************************************************************************************************************/
package de.sanandrew.mods.clonecraft.util;

import cpw.mods.fml.client.event.ConfigChangedEvent.OnConfigChangedEvent;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.network.FMLEventChannel;
import cpw.mods.fml.common.network.NetworkRegistry;
import de.sanandrew.mods.clonecraft.crafting.RegistryRecipes;
import de.sanandrew.mods.clonecraft.event.LivingDropsHandler;
import net.minecraftforge.common.MinecraftForge;

@Mod(modid = CloneCraftReloaded.MOD_ID, version = CloneCraftReloaded.VERSION, name = "CloneCraft Reloaded", //guiFactory = TM_Main.MOD_GUI_FACTORY,
     dependencies = "required-after:sapmanpack@[2.0.0,)")
public final class CloneCraftReloaded
{
    public static final String MOD_ID = "clonecraft";
    public static final String VERSION = "2.0.0";
    public static final String MOD_LOG = "CloneCraft";
    public static final String MOD_CHANNEL = "CloneCraftNWCH";
//    public static final String MOD_GUI_FACTORY = "de.sanandrew.mods.claysoldiers.client.gui.ModGuiFactory";

    private static final String MOD_PROXY_CLIENT = "de.sanandrew.mods.clonecraft.client.util.ClientProxy";
    private static final String MOD_PROXY_COMMON = "de.sanandrew.mods.clonecraft.util.CommonProxy";

    @Mod.Instance(CloneCraftReloaded.MOD_ID)
    public static CloneCraftReloaded instance;
    @SidedProxy(modId = CloneCraftReloaded.MOD_ID, clientSide = CloneCraftReloaded.MOD_PROXY_CLIENT, serverSide = CloneCraftReloaded.MOD_PROXY_COMMON)
    public static CommonProxy proxy;
    public static FMLEventChannel channel;

//    public static final EventBus EVENT_BUS = new EventBus();

//    public static CreativeTabs clayTab = new CreativeTabClaySoldiers();


    @EventHandler
    public void modPreInit(FMLPreInitializationEvent event) {
        event.getModMetadata().autogenerated = false;

//        ModConfig.config = new Configuration(event.getSuggestedConfigurationFile());
//        ModConfig.syncConfig();
//
        RegistryItems.initialize();
//        RegistryBlocks.initialize();

        MinecraftForge.EVENT_BUS.register(new LivingDropsHandler());
//        NetworkRegistry.INSTANCE.registerGuiHandler(this, new GuiHandler());
    }

    @EventHandler
    public void modInit(FMLInitializationEvent event) {
        RegistryRecipes.initialize();

        FMLCommonHandler.instance().bus().register(this);

        channel = NetworkRegistry.INSTANCE.newEventDrivenChannel(MOD_CHANNEL);

        proxy.modInit();

//        RegistryEntities.registerEntities(this);
    }

    @SubscribeEvent
    public void onConfigChanged(OnConfigChangedEvent eventArgs) {
        if( eventArgs.modID.equals(MOD_ID) ) {
//            ModConfig.syncConfig();
        }
    }
}
