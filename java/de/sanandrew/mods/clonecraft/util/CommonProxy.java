/*******************************************************************************************************************
 * Authors:   SanAndreasP
 * Copyright: SanAndreasP
 * License:   Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International
 *                http://creativecommons.org/licenses/by-nc-sa/4.0/
 *******************************************************************************************************************/
package de.sanandrew.mods.clonecraft.util;

import de.sanandrew.mods.clonecraft.network.ServerPacketHandler;

public class CommonProxy
{
    public void modInit() {
        CloneCraftReloaded.channel.register(new ServerPacketHandler());
    }
}
