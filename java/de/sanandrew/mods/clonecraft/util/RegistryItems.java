/*******************************************************************************************************************
 * Authors:   SanAndreasP
 * Copyright: SanAndreasP
 * License:   Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International
 *                http://creativecommons.org/licenses/by-nc-sa/4.0/
 *******************************************************************************************************************/
package de.sanandrew.mods.clonecraft.util;

import de.sanandrew.core.manpack.util.helpers.SAPUtils;
import de.sanandrew.mods.clonecraft.item.ItemSyringe;
import de.sanandrew.mods.clonecraft.item.ItemTestTube;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public final class RegistryItems
{
    public static Item syringe;
    public static Item testTube;

    public static void initialize() {
        syringe = new ItemSyringe();
        testTube = new ItemTestTube();

        syringe.setCreativeTab(CreativeTabs.tabTools);
        syringe.setUnlocalizedName(CloneCraftReloaded.MOD_ID + ":syringe");

        testTube.setCreativeTab(CreativeTabs.tabTools);
        testTube.setUnlocalizedName(CloneCraftReloaded.MOD_ID + ":testTube");

        SAPUtils.registerItems(syringe, testTube);
//        particleBox = new ItemParticleBox();
//
//        particleBox.setCreativeTab(CreativeTabs.tabDecorations);
//        particleBox.setUnlocalizedName(PDM_Main.MOD_ID + ":particleBox");
//
//        SAPUtils.registerItems(particleBox);
    }
}
