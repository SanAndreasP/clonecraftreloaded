/*******************************************************************************************************************
 * Authors:   SanAndreasP
 * Copyright: SanAndreasP, SilverChiren and CliffracerX
 * License:   Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International
 *                http://creativecommons.org/licenses/by-nc-sa/4.0/
 *******************************************************************************************************************/
package de.sanandrew.mods.clonecraft.item;

import com.mojang.realmsclient.gui.ChatFormatting;
import de.sanandrew.core.manpack.util.helpers.SAPUtils;
import de.sanandrew.mods.clonecraft.client.util.EntityColorCache;
import de.sanandrew.mods.clonecraft.util.CloneCraftReloaded;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityList;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EntityDamageSource;
import net.minecraft.util.IIcon;

import java.util.List;

public class ItemSyringe
        extends Item
{
    public IIcon bloodIcon;

    public static final String NBT_CONTAMINATED = "contaminated";
    public static final String NBT_ENTITY_CLS = "entity";

    public ItemSyringe() {
        this.setMaxStackSize(8);
        this.setFull3D();
        this.setMaxDamage(16);
    }

    @Override
    public boolean hitEntity(ItemStack stack, EntityLivingBase target, EntityLivingBase attacker) {
        if( !(target instanceof EntityLiving) ) {
            return super.hitEntity(stack, target, attacker);
        }

        NBTTagCompound nbt = stack.getTagCompound();
        if( nbt == null ) {
            nbt = new NBTTagCompound();
            stack.setTagCompound(nbt);
        }

        if( !nbt.getBoolean(NBT_CONTAMINATED) && !nbt.getString(NBT_ENTITY_CLS).equals(target.getClass().getName()) ) {
            target.attackEntityFrom(this.causeSyringeDamage(attacker), target.getMaxHealth() / 2.0F);
            nbt.setString(NBT_ENTITY_CLS, target.getClass().getName());
        }

        return true;
    }

    @Override
    public int getColorFromItemStack(ItemStack stack, int pass) {
        if( pass == 1 && getSavedEntityClass(stack) != null ) {
            return EntityColorCache.getBloodColors(stack.getTagCompound().getString(NBT_ENTITY_CLS)).getValue0();
        }

        return 0xFFFFFF;
    }

    @Override
    public IIcon getIcon(ItemStack stack, int pass) {
        return pass == 1 && (stack.hasTagCompound() && stack.getTagCompound().hasKey(NBT_ENTITY_CLS)) ? this.bloodIcon : this.itemIcon;
    }

    @Override
    public boolean requiresMultipleRenderPasses() {
        return true;
    }

    @Override
    public int getRenderPasses(int metadata) {
        return 2;
    }

    @Override
    public Item getContainerItem() {
        return this;
    }

    @Override
    public void registerIcons(IIconRegister iconRegister) {
        this.itemIcon = iconRegister.registerIcon(CloneCraftReloaded.MOD_ID + ":syringe");
        this.bloodIcon = iconRegister.registerIcon(CloneCraftReloaded.MOD_ID + ":syringe_blood");
    }

    @Override
    @SuppressWarnings("unchecked")
    public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean hasAdvInfo) {
        if( stack.hasTagCompound() && stack.getTagCompound().getBoolean(NBT_CONTAMINATED) ) {
            list.add(ChatFormatting.RED + "contaminated");
        } else {
            Class cls = getSavedEntityClass(stack);
            if( cls == null ) {
                list.add("empty");
            } else {
                list.add(SAPUtils.translatePreFormat("entity.%s.name", EntityList.classToStringMapping.get(cls)));
            }
        }
    }

    @Override
    public boolean hasContainerItem(ItemStack stack) {
        return true;
    }

    @Override
    public ItemStack getContainerItem(ItemStack itemStack) {
        NBTTagCompound nbt = new NBTTagCompound();
        ItemStack containerItem = new ItemStack(this, 1);

        nbt.setBoolean(NBT_CONTAMINATED, true);
        containerItem.setTagCompound(nbt);

        return containerItem;
    }

    private DamageSource causeSyringeDamage(Entity attacker) {
        return new EntityDamageSource(SYRINGE_DMG_TYPE, attacker);
    }

    public static final String SYRINGE_DMG_TYPE = CloneCraftReloaded.MOD_ID + ":syringe";

    public static Class getSavedEntityClass(ItemStack stack) {
        if( stack.hasTagCompound() && stack.getTagCompound().hasKey(NBT_ENTITY_CLS) ) {
            try {
                return Class.forName(stack.getTagCompound().getString(NBT_ENTITY_CLS));
            } catch( ClassNotFoundException e ) {
                return null;
            }
        } else {
            return null;
        }
    }
}
