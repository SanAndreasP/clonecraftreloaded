/*******************************************************************************************************************
 * Authors:   SanAndreasP
 * Copyright: SanAndreasP, SilverChiren and CliffracerX
 * License:   Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International
 *                http://creativecommons.org/licenses/by-nc-sa/4.0/
 *******************************************************************************************************************/
package de.sanandrew.mods.clonecraft.item;

import com.mojang.realmsclient.gui.ChatFormatting;
import de.sanandrew.core.manpack.util.helpers.SAPUtils;
import de.sanandrew.mods.clonecraft.client.util.EntityColorCache;
import de.sanandrew.mods.clonecraft.util.CloneCraftReloaded;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.EntityList;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.IIcon;

import java.util.List;

public class ItemTestTube
        extends Item
{
    public IIcon plasmaIcon;
    public IIcon buffyCoatIcon;
    public IIcon erythrocytesIcon;

    public static final String NBT_CONTAMINATED = "contaminated";
    public static final String NBT_REFINED = "refined";
    public static final String NBT_ENTITY_CLS = "entity";

    public ItemTestTube() {
        this.setMaxStackSize(8);
        this.setMaxDamage(16);
    }

    @Override
    public int getColorFromItemStack(ItemStack stack, int pass) {
        if( pass != 0 && stack.hasTagCompound() ) {
            NBTTagCompound nbt = stack.getTagCompound();
            if( nbt.getBoolean(NBT_REFINED) ) {
                if( pass == 1 ) {
                    return EntityColorCache.getBloodColors(stack.getTagCompound().getString(NBT_ENTITY_CLS)).getValue1();
                } else if( pass == 3 ) {
                    return EntityColorCache.getBloodColors(stack.getTagCompound().getString(NBT_ENTITY_CLS)).getValue0();
                }
            } else {
                return EntityColorCache.getBloodColors(stack.getTagCompound().getString(NBT_ENTITY_CLS)).getValue0();
            }
        }

        return super.getColorFromItemStack(stack, pass);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean hasAdvInfo) {
        if( stack.hasTagCompound() ) {
            if( stack.getTagCompound().getBoolean(NBT_CONTAMINATED) ) {
                list.add(ChatFormatting.RED + "contaminated");
            } else {
                Class cls = getSavedEntityClass(stack);
                if( cls == null ) {
                    list.add("empty");
                } else {
                    list.add(SAPUtils.translatePostFormat("entity.%s.name", EntityList.classToStringMapping.get(cls)));
                }
            }
        } else {
            list.add("empty");
        }
    }

    @Override
    public void registerIcons(IIconRegister iconRegister) {
        this.itemIcon = iconRegister.registerIcon(CloneCraftReloaded.MOD_ID + ":tube");
        this.plasmaIcon = iconRegister.registerIcon(CloneCraftReloaded.MOD_ID + ":tube_plasma");
        this.buffyCoatIcon = iconRegister.registerIcon(CloneCraftReloaded.MOD_ID + ":tube_buffycoat");
        this.erythrocytesIcon = iconRegister.registerIcon(CloneCraftReloaded.MOD_ID + ":tube_erythrocytes");
    }

    @Override
    public IIcon getIcon(ItemStack stack, int pass) {
        if( getSavedEntityClass(stack) != null ) {
            if( pass == 1 ) {
                return this.plasmaIcon;
            } else if( pass == 2 ) {
                return this.buffyCoatIcon;
            } else if( pass == 3 ) {
                return this.erythrocytesIcon;
            }
        }

        return this.itemIcon;
    }

    @Override
    public boolean requiresMultipleRenderPasses() {
        return true;
    }

    @Override
    public int getRenderPasses(int metadata) {
        return 4;
    }

    public static Class getSavedEntityClass(ItemStack stack) {
        if( stack.hasTagCompound() && stack.getTagCompound().hasKey(NBT_ENTITY_CLS) ) {
            try {
                return Class.forName(stack.getTagCompound().getString(NBT_ENTITY_CLS));
            } catch( ClassNotFoundException e ) {
                return null;
            }
        } else {
            return null;
        }
    }
}
