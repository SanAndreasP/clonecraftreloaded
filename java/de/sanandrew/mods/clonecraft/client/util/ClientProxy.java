/*******************************************************************************************************************
 * Authors:   SanAndreasP
 * Copyright: SanAndreasP
 * License:   Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International
 *                http://creativecommons.org/licenses/by-nc-sa/4.0/
 *******************************************************************************************************************/
package de.sanandrew.mods.clonecraft.client.util;

import de.sanandrew.mods.clonecraft.network.ClientPacketHandler;
import de.sanandrew.mods.clonecraft.util.CloneCraftReloaded;
import de.sanandrew.mods.clonecraft.util.CommonProxy;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.IReloadableResourceManager;

public class ClientProxy
    extends CommonProxy
{
    public static int particleBoxRenderId;

    @Override
    public void modInit() {
        super.modInit();

        CloneCraftReloaded.channel.register(new ClientPacketHandler());

        ((IReloadableResourceManager) Minecraft.getMinecraft().getResourceManager()).registerReloadListener(new EntityColorCache());
//        RegistryEntities.registerRenderers();
    }
}
