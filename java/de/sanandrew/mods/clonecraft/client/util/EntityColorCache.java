/*******************************************************************************************************************
 * Authors:   SanAndreasP
 * Copyright: SanAndreasP, SilverChiren and CliffracerX
 * License:   Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International
 *                http://creativecommons.org/licenses/by-nc-sa/4.0/
 *******************************************************************************************************************/
package de.sanandrew.mods.clonecraft.client.util;

import com.google.common.collect.Maps;
import com.google.common.primitives.Floats;
import de.sanandrew.core.manpack.util.SAPReflectionHelper;
import de.sanandrew.core.manpack.util.helpers.SAPUtils;
import de.sanandrew.core.manpack.util.javatuples.Pair;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.resources.IResourceManager;
import net.minecraft.client.resources.IResourceManagerReloadListener;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import org.apache.commons.lang3.ArrayUtils;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

public final class EntityColorCache
        implements IResourceManagerReloadListener
{
    private static Map<String, Pair<Integer, Integer>> entityClrCache = Maps.newHashMap();

    public static Pair<Integer, Integer> getBloodColors(String entityClsName) {
        if( entityClrCache.containsKey(entityClsName) ) {
            return entityClrCache.get(entityClsName);
        } else {
            try {
                Class<?> eCls = Class.forName(entityClsName);
                Render r = RenderManager.instance.getEntityClassRenderObject(eCls);
                Entity e = (Entity) eCls.getConstructor(World.class).newInstance(Minecraft.getMinecraft().theWorld);
                Object rlObj = SAPReflectionHelper.invokeCachedMethod(Render.class, r, "getEntityTexture", "getEntityTexture", new Class[] { Entity.class },
                                                                      new Object[] { e }
                );
                int clr = AverageColorHelper.getAverageColor((ResourceLocation) rlObj);

                Pair<Integer, Integer> ret = Pair.with(clr, shiftColorHue(clr, 60));
                entityClrCache.put(entityClsName, ret);

                return ret;
            } catch( ClassNotFoundException | InvocationTargetException | NoSuchMethodException | InstantiationException | IllegalAccessException e ) {
                e.printStackTrace();
            }
        }

        return Pair.with(0xFF0000, 0xFFFF00);
    }

    public static int shiftColorHue(int color, int hueShift) {
        int[] hsb = getHsbFromColor(color);
        hsb[0] += hueShift;

        if( hsb[0] < 0 ) {
            hsb[0] += 360;
        } else if( hsb[0] >= 360 ) {
            hsb[0] -= 360;
        }

        return getColorFromHsb(hsb);
    }

    public static int setColorSaturation(int color, int saturation) {
        int[] hsb = getHsbFromColor(color);
        hsb[1] = saturation;

        return  getColorFromHsb(hsb);
    }

    @SuppressWarnings("FloatingPointEquality")
    private static int[] getHsbFromColor(int color) {
        float[] splitColors = ArrayUtils.remove(SAPUtils.getRgbaFromColorInt(color).getColorFloatArray(), 3);  // don't need the alpha value (3)

        float max = Floats.max(splitColors);
        float min = Floats.min(splitColors);

        int hue = 0;
        int sat;
        int bright;

        if( max == min ) {
            hue = 0;
        } else if( max == splitColors[0] ) {
            hue = Math.round(60.0F * ((splitColors[1] - splitColors[2]) / (max - min)));
        } else if( max == splitColors[1] ) {
            hue = Math.round(60.0F * (2.0F + (splitColors[2] - splitColors[0]) / (max - min)));
        } else if( max == splitColors[2] ) {
            hue = Math.round(60.0F * (4.0F + (splitColors[0] - splitColors[1]) / (max - min)));
        }

        if( hue < 0 ) {
            hue += 360;
        } else if( hue >= 360 ) {
            hue -= 360;
        }

        if( max <= 0.01F ) {
            sat = 0;
        } else {
            sat = Math.round((max - min) / max * 100.0F);
        }

        bright = Math.round(max * 100.0F);

        return new int[] {hue, sat, bright};
    }

    public static int getColorFromHsb(int[] hsb) {
        int pickedColor = 0xFF000000;

        int invHue = 360 - hsb[0];

        if( invHue >= 0 && invHue < 60 ) {
            pickedColor = 0xFFFF0000 | ((int) (0xFF * invHue / 60.0F));
        } else if( invHue >= 60 && invHue < 120 ) {
            pickedColor = 0xFF0000FF | ((int) (0xFF - 0xFF * (invHue - 60) / 60.0F) << 16);
        } else if( invHue >= 120 && invHue < 180 ) {
            pickedColor = 0xFF0000FF | ((int) (0xFF * (invHue - 120) / 60.0F) << 8);
        } else if( invHue >= 180 && invHue < 240 ) {
            pickedColor = 0xFF00FF00 | ((int) (0xFF - 0xFF * (invHue - 180) / 60.0F));
        } else if( invHue >= 240 && invHue < 300 ) {
            pickedColor = 0xFF00FF00 | ((int) (0xFF * (invHue - 240) / 15.0F) << 16);
        } else if( invHue >= 300 && invHue < 360 ) {
            pickedColor = 0xFFFF0000 | ((int) (0xFF - 0xFF * (invHue - 300) / 60.0F) << 8);
        }

        float satFloat = 1.0F - hsb[1] / 100.0F;
        int redPart = (pickedColor >> 16) & 0xFF;
        int redSat = (int) ((0xFF - redPart) * satFloat);
        redPart += redSat;

        int greenPart = (pickedColor >> 8) & 0xFF;
        int greenSat = (int) ((0xFF - greenPart) * satFloat);
        greenPart += greenSat;

        int bluePart = pickedColor & 0xFF;
        int blueSat = (int) ((0xFF - bluePart) * satFloat);
        bluePart += blueSat;

        int brightColor = redPart << 16 | greenPart << 8 | bluePart;
        float brightFloat = hsb[2] / 100.0F;
        brightColor = 0xFF000000
                | (int) (((brightColor >> 16) & 0xFF) * brightFloat) << 16
                | (int) (((brightColor >> 8) & 0xFF) * brightFloat) << 8
                | (int) ((brightColor & 0xFF) * brightFloat);

        return brightColor;
    }

    /** resets the cache when ResourceManager is reloaded */
    @Override
    public void onResourceManagerReload(IResourceManager resourceMgr) {
        entityClrCache.clear();
    }
}
