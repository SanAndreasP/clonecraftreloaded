/*******************************************************************************************************************
 * Authors:   SanAndreasP
 * Copyright: SanAndreasP, SilverChiren and CliffracerX
 * License:   Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International
 *                http://creativecommons.org/licenses/by-nc-sa/4.0/
 *******************************************************************************************************************/
package de.sanandrew.mods.clonecraft.event;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import de.sanandrew.core.manpack.util.SAPReflectionHelper;
import de.sanandrew.mods.clonecraft.item.ItemSyringe;
import net.minecraft.entity.EntityLivingBase;
import net.minecraftforge.event.entity.living.LivingDropsEvent;

public class LivingDropsHandler
{
    @SubscribeEvent
    public void onLivingDrops(LivingDropsEvent event) {
        if( event.source.damageType.equals(ItemSyringe.SYRINGE_DMG_TYPE) ) {
            // TODO: check MCP field value!
            SAPReflectionHelper.setCachedFieldValue(EntityLivingBase.class, event.entityLiving, "recentlyHit", "recentlyHit", 0);
            event.setCanceled(true);
        }
    }
}
